#!/usr/bin/env python

import rospy
import time
from dz2_leds.msg import LEDCmd

pub = rospy.Publisher('ledcmd', LEDCmd, queue_size=10)
rospy.init_node('ledtester')

cmd1 = LEDCmd()
cmd1.pattern = LEDCmd.PATTERN_CONST
cmd1.color.g = 150
cmd1.time = 1
cmd1.cycles = 1

cmd2 = LEDCmd()
cmd2.pattern = LEDCmd.PATTERN_FULLFADE
cmd2.color.b = 150
cmd2.time = 1
cmd2.cycles = 3

cmd3 = LEDCmd()
cmd3.pattern = LEDCmd.PATTERN_STROBE
cmd3.color.r = 150
cmd3.time = 1
cmd3.cycles = 3

cmd4 = LEDCmd()
cmd4.pattern = LEDCmd.PATTERN_FULLFADE
cmd4.color.r = 150
cmd4.color.g = 150
cmd4.color.b = 150
cmd4.time = 30
cmd4.cycles = 3

pub.publish(cmd1)
pub.publish(cmd2)
pub.publish(cmd3)
pub.publish(cmd4)
