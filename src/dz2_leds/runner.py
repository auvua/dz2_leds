#!/usr/bin/env python
from dz2_leds.msg import LEDCmd
from std_msgs.msg import ColorRGBA
import threading
import time
import math
import opc

class PatternRunner(threading.Thread):
    def __init__(self, fcconfig, ledcmd):
        threading.Thread.__init__(self)
        self.config = fcconfig
        self.ledcmd = ledcmd
        self.paused = False
        self.interrupted = False
        self.finished = False
        self.client = opc.Client('%s:%d'%(self.config['hostname'], self.config['port']))

    def run(self):
        self.start_time = time.time()
        if self.ledcmd.cycles > 0:
            cycletime = self.ledcmd.time / self.ledcmd.cycles
        else:
            cycletime = self.ledcmd.time
        pixels = [(0,0,0)]*512
        color = self.ledcmd.color

        while not self.interrupted:
            time.sleep(0.005)

            if self.paused:
                continue

            if self.ledcmd.pattern == LEDCmd.PATTERN_CONST:
                color_tup = (color.r, color.g, color.b)
                for channel in range(8):
                    start,end = self.config['channels'][channel]
                    pixels[channel*64+start:channel*64+end+1] = [color_tup]*(end-start)

            elif self.ledcmd.pattern == LEDCmd.PATTERN_FULLFADE:
                scaling = -math.cos(((time.time() - self.start_time) % cycletime) / cycletime * 2 * 3.14159)/2.0 + 0.5

                color_tup = (int(color.r * scaling), int(color.g * scaling), int(color.b * scaling))
                for channel in range(8):
                    start,end = self.config['channels'][channel]
                    pixels[channel*64+start:channel*64+end+1] = [color_tup]*(end-start)

            elif self.ledcmd.pattern == LEDCmd.PATTERN_STROBE:
                if (time.time() - self.start_time) % cycletime > cycletime/2.0:
                    color_tup = (color.r, color.g, color.b)
                else:
                    color_tup = (0,0,0)
                    
                for channel in range(8):
                    start,end = self.config['channels'][channel]
                    pixels[channel*64+start:channel*64+end+1] = [color_tup]*(end-start)

            else:
                pass

            if time.time() - self.start_time > self.ledcmd.time or \
                time.time() - self.start_time > self.config['timeout']:
                break

            self.client.put_pixels(pixels)
        self.client.put_pixels([(0,0,0)]*512)
        self.finished = True

    def pause(self):
        self.paused = True

    def unpause(self):
        self.paused = False

    def interrupt(self):
        self.interrupted = True

    def is_finished(self):
        return self.finished
