#!/usr/bin/env python

import rospy
import time
import threading
import opc
from runner import PatternRunner
from dz2_leds.msg import LEDCmd

fcconfig = {}
commands = []
current_command = None

def ledcmd_callback(msg):
    global commands
    global fcconfig
    commands.append(PatternRunner(fcconfig, msg))
    rospy.logdebug("LED command queued")

def main():
    global fcconfig
    global commands
    global current_command

    subLEDCmd = rospy.Subscriber('ledcmd', LEDCmd, ledcmd_callback)
    rospy.init_node('ledserver', log_level=rospy.DEBUG)
    rate = rospy.Rate(20)

    fcconfig['hostname'] = rospy.get_param('~hostname', 'localhost')
    fcconfig['port'] = rospy.get_param('~port', 7890)
    fcconfig['dither'] = rospy.get_param('~dither', True)
    fcconfig['interpolate'] = rospy.get_param('~interpolate', True)
    fcconfig['map'] = rospy.get_param('~map', [0,0,0,512])
    fcconfig['channels'] = rospy.get_param('~channels', [(0,63)]*8)
    fcconfig['timeout'] = rospy.get_param('~timeout', 60)

    rospy.logdebug("Starting LED server")

    while not rospy.is_shutdown():
        if (current_command is None) and len(commands) > 0:
            current_command = commands.pop(0)
            current_command.start()
            rospy.logdebug("Starting LED command")
        elif current_command is not None:
            if current_command.is_finished():
                current_command = None
                rospy.logdebug("Command completed")

        rate.sleep()
